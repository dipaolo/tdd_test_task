# TDD Test Task

This is a test task performed by Pavel 'DiPaolo' Ditenbir specially for GPAC.

### Test Task Instructions

- You have to write an executable that takes one string argument and writes a value to stdout.
- A 'tdd' binary is provided for your platform.
- 'tdd' is a set of tests. Execute them with "./tdd my_executable". You have to pass a maximum of tests while keeping your code clean.
- To start, execute './tdd "echo hello"'. You should see an error saying that the program is expected to return "0" to stdout when the input is "0".
- Then execute './tdd "echo 0"' to fake a program that returns 0, you'll see the next test appear! So your first iteration of the program should print 0 to stdout.

### Build

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

### Tests

```
$ cd tests
$ ./run.sh
```

### License and Copyright

MIT License

Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
