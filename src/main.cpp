//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Main appication entry
//

#include <iostream>
#include "cmdline_parser.h"
#include "math_expression_parser.h"


int main(int argc, char *argv[])
{
    CmdlineParser cmdlineParser(argc, argv);

    MathExpressionParser parser(cmdlineParser.getParam(0), false);
    std::cout << parser.evaluate();

    return 0;
}
