//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Mathematical expression parser. Supports:
//   - digits: dec, hex
//   - operations: '+', '-'
//   - brackets: '(', ')'
//

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <fstream>

#include "math_expression_parser.h"

using namespace std;

static auto coutNull = ofstream("/dev/null");


MathExpressionParser::MathExpressionParser(const string &expression, bool traceMode /* = false */) :
    mOutStream(traceMode ? cout : coutNull),
    mExpression(expression)
{
}

int64_t MathExpressionParser::anyBaseNumberStringToInteger(const string &param)
{
    auto base = [](const string &str) {
        if (str.length() > 1 && str.substr(0, 2) == "0x")
            return 16;
        else if (str.length() > 1 && str[0] == '0')
            return 2;
        else
            return 10;
    }(param);

    size_t pos = 0;
    auto paramInt = stoi(param, &pos, base);
    if (pos > 0)
        return paramInt;

    return 0;
}

string MathExpressionParser::evaluate() const
{
    // evaluate all expressions within brackets first
    string expr(mExpression);
    while (evaluateExpressionsWithinBrackets(expr))
        ;

    // there are no more brackets left; evaluate the expression
    return evaluate(expr);
}

string MathExpressionParser::evaluate(const string &str) const
{
    auto expression = str;

    // remove all spaces
    expression.erase(
                remove_if(expression.begin(), expression.end(), [](char ch) {
                        return ch == ' ';
                    }),
                expression.end());

    enum ParsingState
    {
        NotStarted,
        Number,
        Addition,
        Subtraction
    };

    auto GetStateString = [](ParsingState state) {
        switch (state)
        {
            case NotStarted:  return "Not Started";
            case Number:      return "Number";
            case Addition:    return "Addition";
            case Subtraction: return "Subtraction";
        }
    };

    ParsingState curState = Addition;
    ParsingState prevState = NotStarted;
    ParsingState curOperation = Addition;

    string curNum;

    int result = 0;
    for (const auto &ch : expression)
    {
        prevState = curState;

        if (isxdigit(ch) || ch == 'x')
        {
            curState = Number;
            curNum += ch;
        }
        else
        {
            if (ch == '+')
                curState = Addition;
            else if (ch == '-')
                curState = Subtraction;
        }

        if (curState != prevState)
        {
            if (prevState == Number)
            {
                const auto curValue = stoi(curNum);
                switch (curOperation) {
                    case Addition:
                        result += curValue;
                        curOperation = curState;
                        break;

                    case Subtraction:
                        result -= curValue;
                        curOperation = curState;
                        break;
                }

                curNum = "";
                curOperation = curState;
            }
            else if (curState != Number)
            {
                curOperation = curState;
            }

            mOutStream << "state -> " << GetStateString(curState) <<
                          "  result: " << result << endl;
        }
    }

    if (curOperation == Addition)
        result += anyBaseNumberStringToInteger(curNum);
    else if (curOperation == Subtraction)
        result -= anyBaseNumberStringToInteger(curNum);

    return to_string(result);
}

//
// Returns false if no brackets found and true otherwise
//
bool MathExpressionParser::evaluateExpressionsWithinBrackets(string &expression) const
{
    typedef pair<int, int> SubstrPos;
    const SubstrPos emptySubstr(0, 0);

    int curPos = 0;
    int bracketsDepth = 0;
    SubstrPos substrPos;

    for (const auto &ch : expression)
    {
        if (ch == '(')
        {
            if (bracketsDepth == 0)
                substrPos.first = curPos;
            bracketsDepth++;
        }
        else if (ch == ')')
        {
            bracketsDepth--;
            if (bracketsDepth == 0)
                substrPos.second = curPos;
        }
        curPos++;
    }

    if (substrPos == emptySubstr)
        return false;

    const auto evaluatedStr = evaluate(expression.substr(substrPos.first, substrPos.second - substrPos.first));
    expression.erase(substrPos.first, substrPos.second - substrPos.first + 1);
    expression.insert(substrPos.first, evaluatedStr);

    return true;
}
