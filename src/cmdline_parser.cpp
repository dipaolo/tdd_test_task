//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Command line simple parser; allows only basic functionality
// yet enough for the task
//

#include "cmdline_parser.h"


CmdlineParser::CmdlineParser(int argc, char **argv)
{
    for (int i = 1; i < argc; i++)
        mParameters.push_back(argv[i]);
}

std::string CmdlineParser::getParam(int number) const
{
    return mParameters[number];
}
