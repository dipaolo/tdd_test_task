//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Mathematical expression parser. Supports:
//   - digits: dec, hex
//   - operations: '+', '-'
//   - brackets: '(', ')'
//

#pragma once

#include <string>


class MathExpressionParser
{
public:
    MathExpressionParser(const std::string &expression, bool traceMode = false);
    ~MathExpressionParser() = default;

    static int64_t anyBaseNumberStringToInteger(const std::string &param);

    std::string evaluate() const;

private:
    std::string evaluate(const std::string &str) const;
    bool evaluateExpressionsWithinBrackets(std::string &expression) const;

private:
    std::basic_ostream<char> &mOutStream;
    std::string mExpression;
};
