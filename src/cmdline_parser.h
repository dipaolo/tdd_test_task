//
// MIT License
//
// Copyright (c) 2018 Pavel 'DiPaolo' Ditenbir
//
// E-mail: pavel.ditenbir@gmail.com
//
//
// Command line simple parser; allows only basic functionality
// yet enough for the task
//

#pragma once

#include <string>
#include <vector>


class CmdlineParser
{
public:
    CmdlineParser(int argc, char *argv[]);
    ~CmdlineParser() = default;

    std::string getParam(int number) const;

private:
    std::vector<std::string> mParameters;
};
